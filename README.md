# 面试题目



<b style="color: blue">Java EE 方向</b>



## 1、Java基础




- [面向对象](./java-se-basic/oop.md)
- [基础](./java-se-basic/basic.md)
- [集合](./java-se-basic/collections.md)



## 2、Java高级



- [JVM](./java-se-advanced/jvm.md)
- [IO](./java-se-advanced/io.md)
- [多线程](./java-se-advanced/multithread.md)



## 3、数据库



- [数据库基本理论](./database/db_basic.md)
- [缓存基本理论](./database/cache_basic.md)
- [数据库索引](./database/db-index.md)
- [MySQL](./database/mysql.md)



## 4、Java EE



- [Servlet&JSP](./java-ee/java-web-basic.md)



## 5、MyBatis



- [基础](./mybatis/interview.md)



## 6、Spring



- [基础](./spring/interview.md)